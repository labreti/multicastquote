package client;

import java.io.*;
import java.net.*;

public class MulticastClient {

    private static final int PORT = 4446;
	private static final String GROUP_IP = "230.0.0.1";
	private static final String LOCAL_IP = "192.168.5.1";

	public static void main(String[] args) throws IOException {

		MulticastSocket socket = join();
        // get a few quotes
	for (int i = 0; i < 5; i++) {

	    String received = receive(socket);
            System.out.println("Quote of the Moment: " + received);
	}

	socket.leaveGroup(InetAddress.getByName(GROUP_IP));
	socket.close();
    }

	private static String receive(MulticastSocket socket) throws IOException {
		DatagramPacket packet;
		byte[] buf = new byte[256];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            String received = new String(packet.getData(), 0, packet.getLength());
		return received;
	}

	private static MulticastSocket join() throws UnknownHostException,
			IOException, SocketException {
		    	InetAddress iface = InetAddress.getByName(LOCAL_IP);
		    	MulticastSocket socket = new MulticastSocket(PORT);
		        InetAddress address = InetAddress.getByName(GROUP_IP);
		        socket.setInterface(iface);
		        socket.joinGroup(address);
		return socket;
	}

}