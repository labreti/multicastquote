package server;

import java.io.*;
import java.net.*;
import java.util.*;

public class MulticastServerThread extends Thread {

	private static final int PORT = 4446;
	private static final String LOCAL_IP = "192.168.5.2";
	private static final String GROUP_IP = "230.0.0.1";
	protected MulticastSocket socket = null;
    protected BufferedReader in = null;
    protected boolean moreQuotes = true;


    public MulticastServerThread() throws IOException {

		// Porta di destinazione del multicast
		socket = new MulticastSocket(4445);
    	// Interfaccia locale usata per il multicast
		socket.setInterface(InetAddress.getByName(LOCAL_IP));
		try {
			in = new BufferedReader(new FileReader("one-liners.txt"));
		} catch (FileNotFoundException e) {
			System.err.println("Could not open quote file. Serving time instead.");
		}

	}

	private long FIVE_SECONDS = 5000;


    public void run() {
        while (moreQuotes) {
            try {
                    // construct quote
                String dString = null;
                if (in == null)
                    dString = new Date().toString();
                else
                    dString = getNextQuote();
                send(dString);
        		System.out.println(dString);
		    // sleep for a while
		try {
		    sleep((long)(Math.random() * FIVE_SECONDS));
		} catch (InterruptedException e) { }
            } catch (IOException e) {
                e.printStackTrace();
		moreQuotes = false;
            }
        }
	socket.close();
    }
	private void send(String dString) throws UnknownHostException, IOException {
		byte[] buf;
		buf = dString.getBytes();
		InetAddress group = InetAddress.getByName(GROUP_IP);
		DatagramPacket packet = new DatagramPacket(buf, buf.length, group, PORT);
		socket.send(packet);
	}
	protected String getNextQuote() {
		String returnValue = null;
		try {
			if ((returnValue = in.readLine()) == null) {
				in.close();
				moreQuotes = false;
				returnValue = "No more quotes. Goodbye.";
			}
		} catch (IOException e) {
			returnValue = "IOException occurred in server.";
		}
		return returnValue;
	}
}